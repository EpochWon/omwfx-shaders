sampler_3d water_nrm {
    source = "shaders/textures/water_nrm.dds";
    mag_filter = linear;
    min_filter = linear;
    wrap_s = repeat;
    wrap_t = repeat;
    wrap_r = repeat;
}

shared {
    float lf = (0.2 + 0.8*omw.sunVis) * (0.32*omw.sunColor.r + 0.47*omw.sunColor.g + 0.21*omw.sunColor.b);
    const vec4 lightcolour = vec4(0.6, 0.91, 1.0, 0.0);

    vec3 toWorld(vec2 tex)
    {
        vec3 v = vec3(omw.viewMatrix[0][2], omw.viewMatrix[1][2], omw.viewMatrix[2][2]);
        v += vec3(1.0/omw.projectionMatrix[0][0] * (2.0*(1.0 - tex.x)-1.0)) * vec3(omw.viewMatrix[0][0], omw.viewMatrix[1][0], omw.viewMatrix[2][0]);
        v += vec3(-1.0/omw.projectionMatrix[1][1] * (2.0*tex.y-1.0)) * vec3(omw.viewMatrix[0][1], omw.viewMatrix[1][1], omw.viewMatrix[2][1]);
        return v;
    }
}

fragment rays {
    omw_In vec2 omw_TexCoord;

    float fetch(vec3 tex, float uw)
    {
        float c = texture(water_nrm, vec3(tex.xy / 3308, 0.22 * omw.simulationTime)).b;
        return pow(c, 0.25) * clamp(exp(tex.z/90.0), 0.0, 1.0) * clamp(1.0 + tex.z/uw, 0.0, 1.0);
    }

    float getLinearDepth(in vec2 tex)
    {
        float d = omw_GetDepth(tex);
        float ndc = d * 2.0 - 1.0;

        return omw.near * omw.far / (omw.far + ndc * (omw.near - omw.far));
    }

    void main()
    {
        //FIXME: wobble clamping
        vec2 wobble = 0.01 * (2 * texture(water_nrm, vec3(omw_TexCoord, 0.2*omw.simulationTime)).rg - 1);
        // wobble *= 1 - pow(2*omw_TexCoord - 1, vec2(32));
        wobble += omw_TexCoord;

        vec4 c = omw_GetLastShader(wobble);

        float d = getLinearDepth(wobble);
        vec3 v = toWorld(wobble);
        v *= -1;
        vec3 e = vec3(0.1, 0.1, 1.0) * omw.eyePos.xyz;
        vec2 r;

        d = min(d, -omw.eyePos.z/max(1e-5, v.z));
        v = v / length(v.xy);
        r = -omw.sunPos.xy - v.xy * dot(-omw.sunPos.xy, v.xy);

        vec3 shellpos = e + 800 * v;
        shellpos.xy += 0.8 * r * shellpos.z;

        float rayz = fetch(shellpos, min(omw.eyePos.z, 400)) * clamp(d/800, 0.0, 1.0) * 0.294;

        vec3 shellpos2 = e + 1400 * v;
        shellpos2.xy += 1.0 * r * shellpos2.z;

        rayz += fetch(shellpos2, 2*omw.eyePos.z) * clamp(d/800 - 1, 0.0, 1.0) * 0.132;

        vec3 shellpos3 = e + 2000 * v;
        shellpos3.xy += 1.2 * r * shellpos3.z;
        rayz += fetch(shellpos3, 3*omw.eyePos.z) * clamp(d/800 - 2, 0.0, 1.0) * 0.065;

        omw_FragColor = c + rayz * lf * lightcolour;
    }
}

technique {
    passes = rays;
    description = "Underwater effects";
    author = "Hrnchamd";
    version = "1.0";
    flags = Disable_AboveWater;
}
